# https://www.youtube.com/watch?v=1_VWkgAH_AY
# MH mobile host
# MSS mobile support station

from token_node import TokenNode
import collections
from collections import deque


class LogicalRing(deque):
    pass

logical_ring = LogicalRing(maxlen=4)
print(logical_ring)

for i in range(7):
    logical_ring.append(TokenNode("node_"+str(i)))

print(logical_ring)
for i in logical_ring:
    print(i)

node_a = TokenNode("node_a")
node_b = TokenNode("node_b")
node_c = TokenNode("node_c")
node_d = TokenNode("node_d")

logical_ring = []
logical_ring.append(node_a)
logical_ring.append(node_b)
logical_ring.append(node_c)
logical_ring.append(node_d)

token = None # a permission


d = deque(maxlen=4)
print(d)

for i in range(20):
    d.append(i)

print(d)

