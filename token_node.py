from node import Node


class TokenNode(Node):

    def __init__(self, node_id):
        super().__init__(node_id)
        self._token = None

    @property
    def token(self):
        if self._token is not None:
            return self.token
        else:
            return None

    @token.setter
    def token(self, token):
        self._token = token

    def drop_token(self):
        self._token = None